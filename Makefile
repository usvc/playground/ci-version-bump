ssl:
	touch .gitignore
	if [ ! $(cat ./.gitignore | grep '/ssl') ]; then echo '/ssl' >> .gitignore; fi
	mkdir -p ./ssl
	ssh-keygen -t rsa -b 4096 -f ./ssl/id_rsa -q -N ""
	cat ./ssl/id_rsa | base64 -w 0 > ./ssl/id_rsa.base64
	
